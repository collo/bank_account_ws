package daos

import javax.inject.{Inject, Singleton}
import java.util.Calendar
import java.text.SimpleDateFormat
import java.sql.Timestamp
import play.api.db.slick.DatabaseConfigProvider
import scala.concurrent.{Future, Await}
import slick.driver.MySQLDriver.api._
import slick.driver.JdbcProfile
import play.api.libs.json._
import play.api.mvc.{Result}
import scala.concurrent.ExecutionContext.Implicits.global
import play.api.mvc.Results._
import models.{BankAccount, BankAccountTransactions, BankAccountTransactionsTableDef, BankAccountTableDef}


@Singleton
class BankAccountTransactionsDao @Inject()(protected val dbConfigProvider: DatabaseConfigProvider){
    val dbConfig = dbConfigProvider.get[JdbcProfile]

    val transactions = TableQuery[BankAccountTransactionsTableDef]
    val accounts = TableQuery[BankAccountTableDef]
    // list transactions
    def listAll: Future[Seq[BankAccountTransactions]] = {
        val query = transactions.result
        dbConfig.db.run(query)
    }

    def todayTimestamp: Timestamp = {
        var cal = Calendar.getInstance()
        val format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val today = format.format(cal.getTime())
        new Timestamp(format.parse(today).getTime)
    }

    def yesterdaysDate: Timestamp = {
        var cal = Calendar.getInstance()
        val format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        // go back one day
        cal.add(Calendar.DATE, -1)
        val yesterday_date = format.format(cal.getTime())
        new Timestamp(format.parse(yesterday_date).getTime)
    }

    def totalTransactionsToday(bankAccount: BankAccount, transactionType: String):Future[Long] = {
        val query = transactions.filter(_.transactionType === transactionType)
            .filter(_.bankAccountId === bankAccount.id)
            .filter(_.timestamp > this.yesterdaysDate)

        val res = dbConfig.db.run(query.result)
        res.map {result =>
            println(s"totalTransactionsToday : ${result.length.toLong} for bank: ${bankAccount.id}" )
            result.length.toLong
        }

    }

    def totalTransactionsAmountToday(bankAccount: BankAccount, transactionType: String):Future[Double] = {
        val query = transactions.filter(_.transactionType === transactionType)
            .filter(_.bankAccountId === bankAccount.id)
            .filter(_.timestamp > this.yesterdaysDate)

        val res = dbConfig.db.run(query.map(_.amount).sum.result)
        res.map {result =>
            println(s"totalTransactionsAmountToday : ${result.getOrElse(0)} for bank: ${bankAccount.id}" )
            result.getOrElse(0)
        }

    }

    def createDepositTransaction(bankAccount:BankAccount, amount:Double):Future[Result] =  {
        var id = None: Option[Long]
        var transaction = BankAccountTransactions(
            id=id, bankAccountId=bankAccount.id, amount=amount, transactionType="DEPOSIT",
            timestamp=this.todayTimestamp)
        val createTransactionQuery = (transactions returning transactions.map(_.id)) += transaction

         // update balance
        val balance = bankAccount.balance + amount
        val updateBalanceQuery = accounts.filter(_.id === bankAccount.id.toLong).map(
            a => (a.balance)).update((balance))

        val accountDepositOperation = for {
           _ <- updateBalanceQuery
           _ <- createTransactionQuery
        } yield ()
        // run them atomically
        dbConfig.db.run(accountDepositOperation.transactionally).map(res=>
            Ok(Json.obj("message" -> "Deposit successfull"))

        )

    }

    def createWithdrawalTransaction(bankAccount:BankAccount, amount:Double):Future[Result] =  {
        var id = None: Option[Long]
        var transaction = BankAccountTransactions(
            id=id, bankAccountId=bankAccount.id, amount=amount, transactionType="WITHDRAWAL",
            timestamp=this.todayTimestamp)
        val createTransactionQuery = (transactions returning transactions.map(_.id)) += transaction

        val balance = bankAccount.balance - amount
        val updateBalanceQuery = accounts.filter(_.id === bankAccount.id.toLong).map(
            a => (a.balance)).update((balance))

        val accountWithdrawOperation = for {
           _ <- updateBalanceQuery
           _ <- createTransactionQuery
        } yield ()
        // run them atomically
        dbConfig.db.run(accountWithdrawOperation.transactionally).map(res=>
            Ok(Json.obj("message" -> "Withdrawal successful"))
        )

    }
}
