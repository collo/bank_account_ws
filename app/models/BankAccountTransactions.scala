package models

import slick.driver.MySQLDriver.api._
import java.sql.Timestamp
import play.api._
import play.api.libs.json._
import java.text.SimpleDateFormat
import models.BankAccountTableDef

case class BankAccountTransactions(
    id: Option[Long],
    bankAccountId:Long,
    amount:Double,
    transactionType:String,
    timestamp:Timestamp
)


// BankAccountTransactions Singleton - handle serialization
object BankAccountTransactions{
    implicit object bankAccountTransactionsFormat extends Format[BankAccountTransactions]{
        // timestamp formatter
        implicit object TimestampFormat extends Format[Timestamp] {
            val format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            def reads(json: JsValue) = {
                val str = json.as[String]
                JsSuccess(new Timestamp(format.parse(str).getTime))
            }
            def writes(ts: Timestamp) = JsString(format.format(ts))
        }

        // from json to bankAccountTransactions object
        override def reads(json: JsValue): JsSuccess[BankAccountTransactions] = JsSuccess( new BankAccountTransactions(
            (json \ "id").asOpt[Long],
            (json \ "bankAccountId").as[Long],
            (json \ "amount").as[Double],
            (json \ "transactionType").as[String],
            (json \ "timestamp").as[Timestamp]
        ))

        // from bankaccountTransactions objects to json
        override def writes(bankAccTrans: BankAccountTransactions): JsValue = JsObject(List(
             // "id" -> JsNumber(bankAccTrans.id.toLong),
             "bankAccountId" -> JsNumber(bankAccTrans.bankAccountId.toLong),
             "transactionType" -> JsString(bankAccTrans.transactionType),
             "amount" -> JsNumber(bankAccTrans.amount.toLong),
             "timestamp" -> JsString(bankAccTrans.timestamp.toString)
        ))
    }
}


// BankAccountTransactions Table
class BankAccountTransactionsTableDef(tag:Tag) extends Table[BankAccountTransactions](tag, "bank_account_transactions"){
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def bankAccountId = column[Long]("bank_account_id")
    def amount = column[Double]("amount")
    def transactionType = column[String]("transaction_type")
    def timestamp = column[Timestamp]("timestamp", O.SqlType("timestamp not null default CURRENT_TIMESTAMP"))

    def bankAccount = foreignKey("BANK_FK", bankAccountId, TableQuery[BankAccountTableDef])(_.id, onUpdate=ForeignKeyAction.Restrict, onDelete=ForeignKeyAction.Restrict)


    override def * = {
        (id.?, bankAccountId, amount , transactionType , timestamp) <>
            ((BankAccountTransactions.apply _).tupled, (BankAccountTransactions.unapply _))
    }
}
