package models
import play.api.libs.json._


case class BankAccountDepositOrWithdraw(
    amount:Double
)

// BankAccountDepositOrWithdraw Serializer
object BankAccountDepositOrWithdraw{
    implicit object bankAccountDepositOrWithdrawFormat extends Format[BankAccountDepositOrWithdraw]{
        override def reads(json: JsValue): JsSuccess[BankAccountDepositOrWithdraw] =
            JsSuccess( new BankAccountDepositOrWithdraw(
                (json \ "amount").as[Double]
            ))
        override def writes(amount: BankAccountDepositOrWithdraw): JsValue = JsObject(List(
             "amount" -> JsNumber(amount.amount.toLong)
        ))
    }
}
