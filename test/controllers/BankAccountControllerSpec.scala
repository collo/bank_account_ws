package controllers

import java.util.Calendar
import java.text.SimpleDateFormat
import org.specs2.mock.Mockito
import play.api.Environment
import play.api.libs.json._
import play.api.mvc._
import play.api.test._
import scala.concurrent.Future
import java.sql.Timestamp
// import org.specs2.matcher.JsonMatchers

import daos.{BankAccountDao, BankAccountTransactionsDao}
import models.BankAccount


class BankAccountControllerSpec extends PlaySpecification with Mockito with Results {
    implicit val mockedBankAccountDao: BankAccountDao = mock[BankAccountDao]
    implicit val mockedBankAccountTransactionsDao: BankAccountTransactionsDao = mock[BankAccountTransactionsDao]
    val cal = Calendar.getInstance()
    val format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    val today = format.format(cal.getTime())
    val timestamp = new Timestamp(format.parse(today).getTime)

    "BankAccountController" should {
        "list bannk accounts" in new WithBankAccountApplication(){

            val bankAccount = BankAccount(1.toLong, "Test Account", 100, 100, timestamp)

            mockedBankAccountDao.listAll() returns Future.successful(List(bankAccount))

            val result = bankAccountController.listBankAccounts().apply(FakeRequest())
            val bodyText= contentAsString(result)
            // todo - assert bodyText content
            status(result) mustEqual OK

        }


    }
}

class WithBankAccountApplication(implicit mockedBankAccountDao: BankAccountDao,mockedBankAccountTransactionsDao: BankAccountTransactionsDao) extends WithApplication {

  val bankAccountController = new BankAccountController(mockedBankAccountDao, mockedBankAccountTransactionsDao)
}

