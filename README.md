Bank Account Web Service
-------------------------

#Installation Steps
==================

##1. Setup the project source

- Install the play activator plugin and ensure it is added to your bash/shell enviroment  path variables
- Clone the project repository at: https://collo@bitbucket.org/collo/bank_account_ws.git


##2. Setup project database

- Create a mysql database named: 'bank_account'
- Update database access settings in the file: ```<app_dir>/conf/application.conf```
    - Entries to be updated are:
        - slick.dbs.default.db.user = ""
        - slick.dbs.default.db.password = ""

##3. Run the project

- cd into the project root directory and run: ```activator run```
    - This may take longer for the sbt to download project dependencies.
- Navigate to the browser on: ```http://localhost:9000
- You will be prompted to apply database evolutions - Apply them.


###3. API Documentations

- Checking balance:
    Do a ```GET``` request to ```/api/balance/<accountNumber>```

- Deposit:
    Do a ```PUT``` request to ```/api/deposit/<accountNumber>``` with ```{"amount": 300}```

- Withdraw:
    Do a ```PUT``` request to ```/api/withdraw/<accountNumber>``` with ```{"amount": 300}```

NOTE: All the APIs only accepts ```application/json``` content type


###5. Seed Data
- Account numbers to use are : ```1000``` and ```1001``` which are created automatically once evolutions are applied


###6. Running Tests

- Create a mysql database named: 'bank_account_test'
- Run tests as ``` activator test ```


###7. Running Tests with coverage Report

- Run ```activator clean coverage test``` to run test with coverage report
- Generate report by running ``` activator coverageReport```
- You can view the html report by openning the file : ```<project_root>/target/scala-2.11/scoverage-report/index.html``` in the browser
