package daos

import javax.inject.{Inject, Singleton}
import play.api.db.slick.DatabaseConfigProvider
import scala.concurrent.{Future, Await}
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success}
import slick.driver.MySQLDriver.api._
import slick.driver.JdbcProfile
import scala.concurrent.ExecutionContext.Implicits.global
import play.api.mvc.{Result}
import play.api.mvc.Results._
import play.api.libs.json._

import models.{BankAccount, BankAccountTableDef}


@Singleton()
class BankAccountDao @Inject()(protected val dbConfigProvider: DatabaseConfigProvider, bankAccountTransactionsDao: BankAccountTransactionsDao){
    val dbConfig = dbConfigProvider.get[JdbcProfile]

    val accounts = TableQuery[BankAccountTableDef]


    // list accounts
    def listAll(): Future[Seq[BankAccount]] = {
        var query = accounts.result
        dbConfig.db.run(query)
    }
    def getAccount(accountNumber:Long): Future[Option[BankAccount]] = {
        var query = accounts.filter(_.accountNumber === accountNumber).result.headOption
        dbConfig.db.run(query)
    }

    def balance(accountNumber:Long): Future[Option[BankAccount]] = {
        this.getAccount(accountNumber)
    }

    def deposit(accountNumber:Long, amount:Double): Future[Result] = {
        val account = Await.result(this.getAccount(accountNumber), Duration.Inf)
        if (account.isDefined){
            val dayTransactions = Await.result(
                bankAccountTransactionsDao.totalTransactionsToday(account.get, "DEPOSIT"), Duration.Inf)
            if (dayTransactions >= 4){
                Future(BadRequest(Json.obj("error_message" -> "Exceeded  Deposit Transactions per day")))
            }else{
                val dayTransactionsAmount = Await.result(
                    bankAccountTransactionsDao.totalTransactionsAmountToday(account.get, "DEPOSIT"),
                     Duration.Inf)
                if(dayTransactionsAmount > 150000){
                    Future(BadRequest(Json.obj("error_message" -> "Exceeded Maximum Allowed Deposit Per Day")))
                }else{
                    // create transaction
                    bankAccountTransactionsDao.createDepositTransaction(account.get, amount)
                }
            }

        }else {
            Future(NotFound(Json.obj("error_message" -> "Account Not Found")))
        }

    }

    def withdraw(accountNumber:Long, amount:Double): Future[Result] = {
        val account = Await.result(this.getAccount(accountNumber), Duration.Inf)
        if (account.isDefined){
            if(account.get.balance < amount){
                Future(BadRequest(Json.obj("error_message" -> "Insufficient funds on your account to withdraw")))
            }else{
                val dayTransactions = Await.result(
                    bankAccountTransactionsDao.totalTransactionsToday(account.get, "WITHDRAWAL"), Duration.Inf)
                if (dayTransactions >= 3){
                    Future(BadRequest(Json.obj("error_message" -> "Exceeded  Withdrawal Transactions per day")))
                }else{
                    val dayTransactionsAmount = Await.result(
                        bankAccountTransactionsDao.totalTransactionsAmountToday(account.get, "WITHDRAWAL"),
                         Duration.Inf)
                    if(dayTransactionsAmount > 50000){
                        Future(BadRequest(Json.obj("error_message" -> "Exceeded Maximum Withdrawal Per Day")))
                    }else{
                        bankAccountTransactionsDao.createWithdrawalTransaction(account.get, amount)
                    }
                }
            }

        }else{
            Future(NotFound(Json.obj("error_message" -> "Account Not Found")))
        }
    }

}
