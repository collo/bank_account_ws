package models

import slick.driver.MySQLDriver.api._
import java.sql.Timestamp
import play.api._
import play.api.libs.json._
import java.text.SimpleDateFormat

//Bank Account Object

case class BankAccount(
    id: Long,
    accountName:String,
    accountNumber:Long,
    balance:Double,
    openedOn:Timestamp
)

// Bank Account Serializer
object BankAccount{
    implicit object bankAccountFormat extends Format[BankAccount]{
        // timestamp formatter
        implicit object TimestampFormat extends Format[Timestamp] {
            val format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            def reads(json: JsValue) = {
                val str = json.as[String]
                JsSuccess(new Timestamp(format.parse(str).getTime))
            }
            def writes(ts: Timestamp) = JsString(format.format(ts))
        }

        // from json to bank account object
        override def reads(json: JsValue): JsSuccess[BankAccount] = JsSuccess( new BankAccount(
            (json \ "id").as[Long],
            (json \ "accountName").as[String],
            (json \ "accountNumber").as[Long],
            (json \ "balance").as[Double],
            (json \ "openedOn").as[Timestamp]
        ))

        // from bank account objects to json
        override def writes(bankAccount: BankAccount): JsValue = JsObject(List(
             "id" -> JsNumber(bankAccount.id.toLong),
             "accountName" -> JsString(bankAccount.accountName),
             "accountNumber" -> JsNumber(bankAccount.accountNumber.toLong),
             "balance" -> JsNumber(bankAccount.balance.toLong),
             "openedOn" -> JsString(bankAccount.openedOn.toString)
        ))
    }
}


// Bank Account Table
class BankAccountTableDef(tag:Tag) extends Table[BankAccount](tag, "bank_account"){
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def accountName = column[String]("account_name")
    def accountNumber = column[Long]("account_number",  O.SqlType("UNIQUE"))
    def balance = column[Double]("balance")
    def openedOn = column[Timestamp]("opened_on", O.SqlType("timestamp not null default CURRENT_TIMESTAMP"))

    override def * = {
        (id , accountName, accountNumber , balance , openedOn) <>
            ((BankAccount.apply _).tupled, (BankAccount.unapply _))
    }
}
