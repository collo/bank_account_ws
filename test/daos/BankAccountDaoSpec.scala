package daos

import play.api.Application
import play.api.test.{WithApplication, PlaySpecification}
import scala.concurrent.duration.Duration
import scala.concurrent.Await
import java.util.Date
import scala.concurrent.Future


class BankAccountDaoSpec extends PlaySpecification{
    import models.{BankAccount, BankAccountTransactions}

    "BankAccountDao" should {
        def bankAccountDao(implicit app: Application) = Application.instanceCache[BankAccountDao].apply(app)

        def bankAccountTransactionsDao(implicit app: Application) = Application.instanceCache[BankAccountTransactions].apply(app)

        "get all bank accounts" in new WithApplication(){
            val accounts = Await.result(bankAccountDao.listAll(),  Duration.Inf)
            accounts.length === 2
            accounts.head.accountName === "Test Account"
        }

        "get a bank account - found" in new WithApplication(){
            val account = Await.result(bankAccountDao.getAccount(1000),  Duration.Inf)
            account.isDefined === true
            account.get.accountName === "Test Account"
        }

        "get a bank account - not found" in new WithApplication(){
            val account = Await.result(bankAccountDao.getAccount(9000),  Duration.Inf)
            account.isDefined === false
        }


    }

}
