package resources
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.test.{WithApplication, PlaySpecification}
import play.api.test._
import play.api.test.Helpers._
import play.api.libs.json._


class BankAccountResourcesSpec extends PlaySpecification{
    import models.{BankAccount, BankAccountTransactions}
    def appWithMemoryDatabase = new GuiceApplicationBuilder().configure(inMemoryDatabase("test")).build()

    "BankAccountResource" should {
        "query account balance - success" in new WithApplication(appWithMemoryDatabase) {
            val Some(result) = route(app, FakeRequest(GET, "/api/balance/1000"))
            status(result) must equalTo(OK)
            contentType(result) must beSome("application/json")
            contentAsString(result) must contain("balance")
        }

        "query account balance - account not fond" in new WithApplication() {
            val Some(result) = route(app, FakeRequest(GET, "/api/balance/100011"))
            status(result) must equalTo(404)
            contentAsString(result) must contain("Account Not Found")
        }

        "test deposit - successfull" in new WithApplication() {
            val request = FakeRequest(PUT, "/api/deposit/1000").withJsonBody(
                Json.parse("""{ "amount": 1000 }"""))
            val Some(result) = route(app, request)
            status(result) must equalTo(OK)
            contentAsString(result) must contain("Deposit successfull")
        }

        "test deposit - exceeded amount per transaction" in new WithApplication() {
            val request = FakeRequest(PUT, "/api/deposit/1000").withJsonBody(
                Json.parse("""{ "amount": 50000 }"""))
            val Some(result) = route(app, request)
            status(result) must equalTo(400)
            contentAsString(result) must contain("Exceeded Maximum deposit amount per transaction")
        }

        "test withdraw - successfull" in new WithApplication() {
            val request = FakeRequest(PUT, "/api/withdraw/1000").withJsonBody(
                Json.parse("""{ "amount": 1000 }"""))
            val Some(result) = route(app, request)
            status(result) must equalTo(OK)
            contentAsString(result) must contain("Withdrawal successful")
        }

        "test withdraw - exceeded amount per transaction" in new WithApplication() {
            val request = FakeRequest(PUT, "/api/withdraw/1000").withJsonBody(
                Json.parse("""{ "amount": 50000 }"""))
            val Some(result) = route(app, request)
            status(result) must equalTo(400)
            contentAsString(result) must contain("Exceeded Maximum withdraw amount per transaction")
        }
    }

}
