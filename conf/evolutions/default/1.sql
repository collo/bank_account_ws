
# --- !Ups

# BankAccount schema

create table `bank_account` (
  `id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `account_name` VARCHAR(254) NOT NULL,
  `account_number` BIGINT NOT NULL UNIQUE,
  `balance` DOUBLE DEFAULT 0,
  `opened_on`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)Engine=InnoDB;


# BankAccountTransactions schema

create table `bank_account_transactions` (
  `id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `bank_account_id` BIGINT NOT NULL,
  `amount` DOUBLE  NOT NULL,
  `transaction_type` SET('DEPOSIT', 'WITHDRAWAL') NOT NULL,
  `timestamp`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (`bank_account_id`) REFERENCES bank_account(`id`)
)Engine=InnoDB;

# --- !Downs
drop table `bank_account`;
drop table `bank_account_transactions`;
