package controllers

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import com.google.inject.Inject

import play.api.libs.json._
import play.api.mvc._

import daos.{BankAccountDao, BankAccountTransactionsDao}
import models.BankAccountDepositOrWithdraw


class BankAccountController @Inject()(bankAccountDao: BankAccountDao, bankAccountTransactionsDao: BankAccountTransactionsDao) extends Controller{

    def listBankAccounts = Action.async {
        val result = bankAccountDao.listAll
        result.map(msgs => Ok(Json.obj("status" -> "Ok", "messages" -> Json.toJson(msgs))))
    }

    def listlistBankAccountTransactions = Action.async {
        val result = bankAccountTransactionsDao.listAll
        result.map(msgs => Ok(Json.obj("status" -> "Ok", "messages" -> Json.toJson(msgs))))
    }


    def balance(accountNumber:Long) = Action.async { implicit request =>
        val result = bankAccountDao.balance(accountNumber);
        result.map { accounts =>
            accounts.map { account =>
                Ok(Json.obj("balance" -> account.balance))
                }.getOrElse(NotFound(Json.obj("error_message" -> "Account Not Found")))
        }

    }

    def deposit(accountNumber:Long) = Action.async(BodyParsers.parse.json) { request =>
        try{
            val depositAmount = request.body.validate[BankAccountDepositOrWithdraw]
            depositAmount.fold(
                invalid => {
                    Future(BadRequest(Json.obj("error" -> invalid.head.toString)))
                },
                valid => {
                    // validate maximum amount - could as well be done om the model
                    if(valid.amount >40000){
                        Future(BadRequest(Json.obj("error_message" -> "Exceeded Maximum deposit amount per transaction")))
                    }else{
                        bankAccountDao.deposit(accountNumber, valid.amount)
                    }
                }
            )
        }catch{
            case e: JsResultException => Future(BadRequest(Json.obj("error" -> "Invalid Data")))
        }

    }

    def withdraw(accountNumber:Long) = Action.async(BodyParsers.parse.json) { request =>
        try{
            val withdrawAmount = request.body.validate[BankAccountDepositOrWithdraw]
            withdrawAmount.fold(
                invalid => {
                    Future(BadRequest(Json.obj("error" -> invalid.head.toString)))
                },
                valid => {
                    // validate maximum amount - could as well be done om the model
                    if(valid.amount >20000){
                        Future(BadRequest(Json.obj("error_message" -> "Exceeded Maximum withdraw amount per transaction")))
                    }else{
                        bankAccountDao.withdraw(accountNumber, valid.amount)
                    }
                }
            )
        }catch{
            case e: JsResultException => Future(BadRequest(Json.obj("error" -> "Invalid Data")))
        }

    }

}
